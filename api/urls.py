from django.urls import include, path

urlpatterns = [
    path('users/', include('users.urls')),
    path('user/', include('rest_auth.urls')),
    path('user/registration/', include('rest_auth.registration.urls')),

]