"""defines the forms to be used in the app"""
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import CustomUser

class CustomUserCreationForm(UserCreationForm):
    """form for the user"""

    class Meta(UserCreationForm.Meta):
        model = CustomUser
        fields = ('username', 'firstname', 'lastname', 'phone', 'password')

class CustomUserChangeForm(UserChangeForm):
    """ change form"""
    class Meta:
        model = CustomUser
        fields = UserChangeForm.Meta.fields