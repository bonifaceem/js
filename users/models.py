""" Create your models here."""

from django.db import models
from django.contrib.auth.models import AbstractUser
from django.conf import settings


class CustomUser(AbstractUser):
    """reps the user model"""
    username = models.EmailField(max_length=255, unique=True)
    firstname = models.CharField(max_length=255)
    lastname = models.CharField(max_length=255)
    phone = models.IntegerField(blank=False)
    password = models.CharField(max_length=255)

    def __str__(self):
        return self.username
        # , self.firstname, self.lastname, self.phone, make_password(self.password)

class Company(models.Model):
    """represents the company model"""
    name = models.CharField(max_length=255, unique=True)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL,
                                related_name='usercompany',
                                on_delete=models.CASCADE)
    company_email = models.EmailField(max_length=255)
    company_description = models.TextField()
    company_services = models.TextField()
    industry_category = models.CharField(max_length=255)
    company_slogan = models.CharField(max_length=255)
    founded_date = models.DateField()
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name

class Job(models.Model):
    """a representation of the job model"""
    title = models.CharField(max_length=255)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL,
                                related_name='userjob',
                                on_delete=models.CASCADE)
    job_summary = models.TextField()
    company_story = models.TextField()
    why_be_interested = models.TextField()
    job_responsibilities = models.TextField()
    how_to_apply = models.TextField()

    def __str__(self):
        return self.title

class EducationProfile(models.Model):
    """represents the profile model"""
    
    education_institution = models.CharField(max_length=255)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL,
                                related_name='usereducation',
                                on_delete=models.CASCADE)
    year = models.DateField()
    level = models.CharField(max_length=255)
    title = models.CharField(max_length=255)
    description = models.CharField(max_length=255)

    def __str__(self):
        return self.education_institution


class CareerProfile(models.Model):
    """the career info model"""
    title = models.CharField(max_length=255)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL,
                                related_name='usercareer',
                                on_delete=models.CASCADE)
    organization = models.CharField(max_length=255)
    start_date = models.DateField()
    end_date = models.DateField()
    location = models.CharField(max_length=255)
    position_description = models.CharField(max_length=255)


    def __str__(self):
        return self.title

class SkillsProfile(models.Model):
    """additional skills' and talents' model"""
    professional_skills = models.CharField(max_length=255)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL,
                                related_name='userskills',
                                on_delete=models.CASCADE)
    extra_skills = models.CharField(max_length=255)
    hobbies = models.CharField(max_length=255)
    interests = models.CharField(max_length=255)

    def __str__(self):
        return self.professional_skills
        