"""urls for the users app"""
from django.urls import path
from django.conf.urls import url

from . import views

urlpatterns = [
    path('', views.UserListView.as_view(), name='users'),
    url(r'^user/(?P<pk>[0-9]+)/$', views.UserDetailsView.as_view(), name='userdetails'),
    path('company', views.CompanyCreateView.as_view(), name='company'),
    url(r'^company/(?P<pk>[0-9]+)/$', views.CompanyDetailsView.as_view(), name='details'),
    path('job', views.JobCreateView.as_view(), name='job')
]