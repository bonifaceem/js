from django.test import TestCase
from django.urls import reverse
from rest_framework.test import APIClient
from rest_framework import status
from . models import Company

class ModelTestCase(TestCase):
    """This will define the testcases for our companies"""

    def setUp(self):
        self.client = APIClient()
        self.user_data = {'username' : 'kar@gmail.com',
                            'firstname': 'Karis',
                            'lastname': 'Maina',
                            'phone' : '876876',
                            'password': 'missmimi'
        }
        self.response = self.client.post(reverse('users'), self.user_data, format='json')

    def test_can_create_company(self):
        """test company creation"""
        self.assertEqual(self.response.status_code, status.HTTP_201_CREATED)

class ViewTestCase(TestCase):
    """this is the test case for the API views"""

    def setUp(self):
        self.client = APIClient()
        self.company_data = {'name': 'Safaricom',
                            'company_email': 'karisbb@gmail.com',
                            'company_description': 'gidfjsdlkjs',
                            'industry_category' : 'jskdfnsjd',
                            'company_slogan': 'kjsdhlkue',
                            'founded_date': "1212-12-12"}
        self.response = self.client.post(reverse('company'), self.company_data, format='json')

    def test_api_can_create_company(self):
        """testing that the API can create a company"""
        self.assertEqual(self.response.status_code, status.HTTP_201_CREATED)

    def test_api_can_get_company(self):
        """test company retrieval"""
        company = Company.objects.get()
        response = self.client.get(reverse('details', kwargs={'pk':company.id}), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, company)

    def test_api_can_update_company_name(self):
        """this will test for company name update"""
        company = Company.objects.get()
        change_company = {'name': 'Airtel'}
        response = self.client.put(reverse('details', kwargs={'pk': company.id}),
                                    change_company,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_api_can_delete_company(self):
        """testing for company deletion"""
        company = Company.objects.get()
        response = self.client.delete(reverse('details', kwargs={'pk':company.id}),
                                                format='json',
                                                follow=True)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
