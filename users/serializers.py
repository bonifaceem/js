from rest_framework import serializers
from . import models
class UserSerializer(serializers.ModelSerializer):
    """override the create method so as to hash the password"""
    def create(self, validated_data):
        password = validated_data.pop('password', None)
        instance = self.Meta.model(**validated_data)
        if password is not None:
            instance.set_password(password)
        instance.save()
        return instance

    class Meta:
        model = models.CustomUser
        fields = ('username', 'firstname', 'lastname', 'phone', 'password')
        # extra_kwargs = {'password' : {'write_only' : True}}

class CompanySerializer(serializers.ModelSerializer):
    """this will map the model instance into JSON format"""

    owner = serializers.ReadOnlyField(source='owner.username')

    class Meta:
        """this will map the instance with the fields specified here"""
        model = models.Company
        db_table = 'users_company'
        fields = ('name', 'owner', 'company_email', 'company_description', 'industry_category', 'company_slogan', 'founded_date', 'created_at', 'modified_at')
        extra_kwargs = {'owner' : {'write_only' : True}}


class JobSerializer(serializers.ModelSerializer):
    """this is the job serializer"""
    owner = serializers.ReadOnlyField(source='owner.username')

    class Meta:
        """maps instance with fields specified"""

        model = models.Job
        fields = ('title', 'owner', 'job_summary', 'company_story', 'why_be_interested', 'job_responsibilities', 'how_to_apply')
        extra_kwargs = {'owner':{'write_only':True}}


class EducationProfileSerializer(serializers.ModelSerializer):
    """this will map the education profile model instance """
    owner = serializers.ReadOnlyField(source='owner.username')

    class Meta:
        """this maps the instance with fields specified"""

        model = models.EducationProfile
        fields = ('education_institution', 'year', 'level', 'title', 'description')
        extra_kwargs={'owner':{'write_only':True}}

