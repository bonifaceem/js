from rest_framework import generics

from . import models
from . import serializers

class UserListView(generics.ListCreateAPIView):
    """defines the user create/read operations"""
    queryset = models.CustomUser.objects.all()
    serializer_class = serializers.UserSerializer

class UserDetailsView(generics.RetrieveUpdateDestroyAPIView):
    """defines update/delete operations for a user account"""
    queryset = models.CustomUser.objects.all()
    serializer_class = serializers.UserSerializer

class CompanyCreateView(generics.ListCreateAPIView):
    """this defines the company create/read behavior"""
    queryset = models.Company.objects.all()
    serializer_class = serializers.CompanySerializer

    def perform_create(self, serializer):
        """this saves the company data on save"""
        serializer.save(owner=self.request.user)

class CompanyDetailsView(generics.RetrieveUpdateDestroyAPIView):
    """this will handle all update/delete operations for company"""
    queryset = models.Company.objects.all()
    serializer_class = serializers.CompanySerializer

class JobCreateView(generics.ListCreateAPIView):
    """defines the job create/read operations"""
    queryset = models.Job.objects.all()
    serializer_class = serializers.JobSerializer

    def perform_create(self, serializer):
        """saves job data on save"""
        serializer.save(owner=self.request.user)

class JobDetailsView(generics.RetrieveUpdateDestroyAPIView):
    """defines update/delete operations"""
    queryset = models.Job.objects.all()
    serializer_class = serializers.JobSerializer

class EducationCreateView(generics.ListCreateAPIView):
    """defines the educ create/read operations"""
    queryset = models.EducationProfile.objects.all()
    serializer_class = serializers.EducationProfileSerializer

    def perform_create(self, serializer):
        """saves with user specified"""
        serializer.save(owner=self.request.user)

class EducationDetailsView(generics.RetrieveUpdateDestroyAPIView):
    """defines update/delete operations for ed profile"""
    queryset = models.EducationProfile.objects.all()
    serializer_class = serializers.EducationProfileSerializer
